package com.pitang.anderson.duarte.dto;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.pitang.anderson.duarte.model.Car;

import lombok.Data;



/**
 * Instantiates a new car DTO.
 */

/**
 * Instantiates a new car DTO.
 */
@Data
public class CarDTO {

	/** The car id. */
	private Long carId;
	
	/** The year. */
	private Integer year;
	
	/** The license plate. */
	private String licensePlate;
	
	/** The model. */
	private String model;
	
	/** The color. */
	private String color;
	
	/** The photo directory. */
	private String photoDirectory;

	/**
	 * Convert entity.
	 *
	 * @param car the car
	 * @return the car
	 */
	public static Car convertEntity(CarDTO car) {
		Mapper mapper = DozerBeanMapperBuilder.buildDefault();
		return mapper.map(car, Car.class);
	}
}
