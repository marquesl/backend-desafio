package com.pitang.anderson.duarte.exception;


/**
 * The Class UploadException.
 */
public class UploadException extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The code. */
	private final int code;
	
	/**
	 * Instantiates a new upload exception.
	 */
	public UploadException() {
	    this(ErrorCode.INVALID_FIELDS);
	}

	/**
	 * Instantiates a new upload exception.
	 *
	 * @param msg the msg
	 */
	public UploadException(String msg, int code) {
		super(msg);
		this.code = code;
	}

	/**
	 * Instantiates a new upload exception.
	 *
	 * @param msg the msg
	 * @param cause the cause
	 */
	public UploadException(String msg, Throwable cause, int code) {
		super(msg, cause);
		this.code = code;
	}
	
	/**
	 * Instantiates a new upload exception.
	 *
	 * @param errorCode the error code
	 */
	public UploadException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
    }
	
	/**
	 * Instantiates a new upload exception.
	 *
	 * @param msg the msg
	 * @param errorCode the error code
	 */
	public UploadException(String msg, ErrorCode errorCode) {
	    super(msg);
	    this.code = errorCode.getCode();
	}
	
	/**
	 * Instantiates a new upload exception.
	 *
	 * @param msg the msg
	 * @param cause the cause
	 * @param errorCode the error code
	 */
	public UploadException(String msg, Throwable cause, ErrorCode errorCode) {
        super(msg, cause);
        this.code = errorCode.getCode();
    }
	
	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
        return code;
    }
}
