package com.pitang.anderson.duarte.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pitang.anderson.duarte.dto.CarDTO;
import com.pitang.anderson.duarte.enums.TypeUploadEnum;
import com.pitang.anderson.duarte.model.Car;
import com.pitang.anderson.duarte.service.CarService;
import com.pitang.anderson.duarte.utils.ApplicationUtils;

import io.swagger.annotations.Api;


/**
 * The Class CarController.
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("cars")
@Api(value = "Car")
public class CarController {

	/** The car service. */
	private CarService carService;

	/**
	 * User service.
	 *
	 * @param carService the car service
	 */
	@Autowired
	public void userService(CarService carService) {
		this.carService = carService;
	}

	/**
	 * Gets the all cars logged user.
	 *
	 * @param authentication the authentication
	 * @return the all cars logged user
	 */
	@GetMapping
	public ResponseEntity<List<CarDTO>> getAllCarsLoggedUser(Authentication authentication) {
		return ResponseEntity
				.ok(ApplicationUtils.mapBeans(carService.getAllCarsByUser(authentication.getName()), CarDTO.class));
	}

	/**
	 * Creates the car by logged user.
	 *
	 * @param authentication the authentication
	 * @param userDto        the user dto
	 * @return the response entity
	 */
	@PostMapping
	public ResponseEntity<CarDTO> createCarByLoggedUser(Authentication authentication, @RequestBody CarDTO userDto) {
		Car car = CarDTO.convertEntity(userDto);
		return ResponseEntity
				.ok(ApplicationUtils.mapBean(carService.saveCarByUser(authentication.getName(), car), CarDTO.class));
	}

	/**
	 * Gets the car by logged user.
	 *
	 * @param authentication the authentication
	 * @param id             the id
	 * @return the car by logged user
	 */
	@GetMapping("/{id}")
	public ResponseEntity<CarDTO> getCarByLoggedUser(Authentication authentication, @PathVariable("id") Integer id) {
		Car car = carService.findByCarIdAndUser(id, authentication.getName());
		return ResponseEntity.ok(ApplicationUtils.mapBean(car, CarDTO.class));
	}

	/**
	 * Delete car by logged user.
	 *
	 * @param authentication the authentication
	 * @param id             the id
	 * @return the response entity
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteCarByLoggedUser(Authentication authentication, @PathVariable("id") int id) {
		carService.deleteCarByIdAndUser(id, authentication.getName());
		return ResponseEntity.noContent().build();
	}

	/**
	 * Update car by logged user.
	 *
	 * @param authentication the authentication
	 * @param id             the id
	 * @param carDTO         the car DTO
	 * @return the response entity
	 */
	@PutMapping("/{id}")
	public ResponseEntity<CarDTO> updateCarByLoggedUser(Authentication authentication, @PathVariable("id") Integer id,
			@RequestBody CarDTO carDTO) {
		Car car = carService.updateCarByIdAndUser(id, CarDTO.convertEntity(carDTO), authentication.getName());
		return ResponseEntity.ok(ApplicationUtils.mapBean(car, CarDTO.class));
	}

	/**
	 * Upload.
	 *
	 * @param file           the file
	 * @param id             the id
	 * @param authentication the authentication
	 * @return the response entity
	 */
	@PostMapping("/upload/{id}")
	public ResponseEntity<CarDTO> uploadCar(@RequestParam("file") MultipartFile file, @PathVariable("id") Integer id,
			Authentication authentication) {
		Car car = carService.uploadCarImage(file, id, authentication.getName(), TypeUploadEnum.CAR);
		return ResponseEntity.ok(ApplicationUtils.mapBean(car, CarDTO.class));
	}

}
