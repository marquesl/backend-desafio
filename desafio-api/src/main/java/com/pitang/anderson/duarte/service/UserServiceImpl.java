/*
 * 
 */
package com.pitang.anderson.duarte.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.pitang.anderson.duarte.enums.TypeUploadEnum;
import com.pitang.anderson.duarte.exception.DataAlreadyFoundException;
import com.pitang.anderson.duarte.exception.DataNotFoundException;
import com.pitang.anderson.duarte.exception.ErrorCode;
import com.pitang.anderson.duarte.exception.InvalidFieldsException;
import com.pitang.anderson.duarte.exception.MissingFieldsException;
import com.pitang.anderson.duarte.exception.UploadException;
import com.pitang.anderson.duarte.model.User;
import com.pitang.anderson.duarte.repository.UserRepository;
import com.pitang.anderson.duarte.util.PitangValidator;

/**
 * The Class UserServiceImpl.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

	/** The user repository. */
	private UserRepository userRepository;
	
	/** The upload service. */
	private UploadService uploadService;

	/**
	 * Instantiates a new user service impl.
	 *
	 * @param userRepository the user repository
	 * @param uploadService the upload service
	 */
	@Autowired
	public UserServiceImpl(UserRepository userRepository, UploadService uploadService) {
		this.userRepository = userRepository;
		this.uploadService = uploadService;
	}

	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	/**
	 * Save user.
	 *
	 * @param user the user
	 * @return the user
	 */
	@Override
	public User saveUser(User user) {
		checkExistUser(user);
		checkMissingFields(user);
		checkInvalidFields(user);
		user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
		user.setCreateAt(new Date());
		return userRepository.save(user);
	}

	/**
	 * Upload user image.
	 *
	 * @param file the file
	 * @param login the login
	 * @param uploadEnum the upload enum
	 * @return the user
	 */
	@Override
	public User uploadUserImage(MultipartFile file, String login, TypeUploadEnum uploadEnum) {
		User user = findByLogin(login);
		try {
			String uploadPhoto = uploadService.uploadPhoto(file, login, uploadEnum);
			user.setPhotoDirectory(uploadPhoto);
			return userRepository.saveAndFlush(user);
		} catch (Exception e) {
			throw new UploadException(ErrorCode.ERROR_UPLOAD_IMAGE);
		}
	}

	/**
	 * Check exist user.
	 *
	 * @param user the user
	 */
	private void checkExistUser(User user) {
		if (userRepository.findByLogin(user.getLogin()) != null) {
			throw new DataAlreadyFoundException(ErrorCode.USER_ALREADY_FOUND);
		}
		if (userRepository.findByEmail(user.getEmail()) != null) {
			throw new DataAlreadyFoundException(ErrorCode.EMAIL_ALREADY_FOUND);
		}
	}

	/**
	 * Find by login.
	 *
	 * @param login the login
	 * @return the user
	 */
	@Override
	public User findByLogin(String login) {
		return userRepository.findByLogin(login);
	}

	/**
	 * Find user by id.
	 *
	 * @param id the id
	 * @return the user
	 */
	@Override
	public User findUserById(Integer id) {
		return userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(ErrorCode.USER_NOT_FOUND));
	}

	/**
	 * Update user.
	 *
	 * @param id the id
	 * @param newUser the new user
	 * @return the user
	 */
	@Override
	public User updateUser(Integer id, User newUser) {
		User user = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(ErrorCode.USER_NOT_FOUND));
		checkMissingFieldsUpdate(newUser);
		checkInvalidFields(newUser);
		user.setFirstName(newUser.getFirstName());
		user.setLastName(newUser.getLastName());
		user.setEmail(newUser.getEmail());
		user.setBirthday(newUser.getBirthday());
		user.setPhone(newUser.getPhone());
		return userRepository.save(user);
	}

	/**
	 * Delete user.
	 *
	 * @param id the id
	 */
	@Override
	public void deleteUser(Integer id) {
		userRepository.deleteById(id);
	}

	/**
	 * Save last login.
	 *
	 * @param login the login
	 */
	@Override
	public void saveLastLogin(String login) {
		User user = userRepository.findByLogin(login);
		user.setLastLogin(new Date());
		userRepository.saveAndFlush(user);
	}

	/**
	 * Check missing fields.
	 *
	 * @param user the dto
	 */
	public static void checkMissingFields(User user) {
		if (StringUtils.isEmpty(user.getEmail()) || StringUtils.isEmpty(user.getFirstName())
				|| StringUtils.isEmpty(user.getLastName()) || StringUtils.isEmpty(user.getPassword())
				|| StringUtils.isEmpty(user.getLogin()) || StringUtils.isEmpty(user.getPhone())
				|| user.getBirthday() == null) {
			throw new MissingFieldsException();
		}
	}
	
	/**
	 * Check missing fields.
	 *
	 * @param user the dto
	 */
	public static void checkMissingFieldsUpdate(User user) {
		if (StringUtils.isEmpty(user.getEmail()) || StringUtils.isEmpty(user.getFirstName())
				|| StringUtils.isEmpty(user.getLastName()) || StringUtils.isEmpty(user.getPhone())
				|| user.getBirthday() == null) {
			throw new MissingFieldsException();
		}
	}

	/**
	 * Check invalid fields.
	 *
	 * @param user the dto
	 */
	public static void checkInvalidFields(User user) {
		if (!PitangValidator.emailIsValid(user.getEmail()) || !PitangValidator.phoneIsValid(user.getPhone())) {
			throw new InvalidFieldsException();
		}
	}

}
