package com.pitang.anderson.duarte.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.pitang.anderson.duarte.enums.TypeUploadEnum;
import com.pitang.anderson.duarte.model.Car;


/**
 * The Interface CarService.
 */
public interface CarService {

	/**
	 * Gets the all cars by user.
	 *
	 * @param login the login
	 * @return the all cars by user
	 */
	public List<Car> getAllCarsByUser(String login);

	/**
	 * Save car by user.
	 *
	 * @param login the login
	 * @param car the car
	 * @return the car
	 */
	public Car saveCarByUser(String login, Car car);

	/**
	 * Update car by id and user.
	 *
	 * @param id the id
	 * @param car the car
	 * @param login the login
	 * @return the car
	 */
	public Car updateCarByIdAndUser(Integer id, Car car, String login);

	/**
	 * Find by license plate.
	 *
	 * @param licensePlate the license plate
	 * @return the car
	 */
	public Car findByLicensePlate(String licensePlate);

	/**
	 * Find by car id and user.
	 *
	 * @param id the id
	 * @param login the login
	 * @return the car
	 */
	public Car findByCarIdAndUser(Integer id, String login);

	/**
	 * Delete car by id and user.
	 *
	 * @param id the id
	 * @param login the login
	 */
	public void deleteCarByIdAndUser(Integer id, String login);

	/**
	 * Upload car image.
	 *
	 * @param file the file
	 * @param id the id
	 * @param login the login
	 * @param car the car
	 * @return the car
	 */
	public Car uploadCarImage(MultipartFile file, Integer id, String login, TypeUploadEnum car);

}
